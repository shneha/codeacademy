export function signup(email, password, doesProducesError = false, error_message) {
    cy.get('#email').click().clear();
    cy.get('#email').type(email);
    if  (password !=='') {
    cy.get('#password').click().clear();
    cy.get('#password').type(password);
    }
    cy.intercept('POST', '/register/validate').as('validate')
    cy.get('.gamut-14y8l6-Box > .e1w6mdco0').click()
    if (doesProducesError) {
        cy.wait('@validate')
        cy.get('.gamut-1q940t8-ErrorSpan > span').should('have.text', error_message); 
    }
}
