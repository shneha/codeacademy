import * as sg from "./signupobject"

describe('signup', () => {
  beforeEach(function () {
    cy.visit('/')
  })
  // Negative scenarios
  // Tests the cases where password failure occurs
  it('password_failure_case', function () {
    cy.fixture('logincases').then(data => {
      cy.log(data)
      const namespace = data.signup_data.password_error;
      const email = namespace.email;
      sg.signup(email, namespace.short, true, 'Password must include at least 8 characters.')
      sg.signup(email, namespace.no_lower, true, 'This password is too weak.')
      sg.signup(email, namespace.no_upper, true, 'This password is too weak.')
      sg.signup(email, namespace.no_number, true, 'This password is too weak.')
      sg.signup(email, namespace.no_symbol, true, 'This password is too weak.')

    })
  })
  // Tests the cases where email failure occurs
  it('email_failure_case', function () {
    cy.fixture('logincases').then(data => {
      cy.log(data)
      const namespace = data.signup_data.email_error;
      const password = namespace.password;
      sg.signup(namespace.no_name, password, true, 'Please enter a valid email address.')
      sg.signup(namespace.no_domain, password, true, 'Please enter a valid email address.')
      sg.signup(namespace.no_com, password, true, 'Please enter a valid email address.')
    })
  })

  // Tests the cases for multiple signup using same email
  it('resignup_case', function () {
    cy.fixture('logincases').then(data => {
      cy.log(data)
      const namespace = data.signup_data.resign;
      const email = namespace.email;
      sg.signup(email, '', true, 'An account with this email already exists.')

    })
  })

  // Positive scenario case
  it('pass_case', function () {
    cy.fixture('logincases').then(data => {
      cy.log(data)
      const namespace = data.signup_data.pass;
      const email = new Date().getTime() + namespace.email;
      const password = namespace.password;
      cy.intercept('POST', '/register/validate').as('validate')
      cy.intercept('GET', '/welcome/find-a-course').as('success')
      sg.signup(email, password)
      cy.wait('@validate')
      cy.wait('@success', { timeout:10000 })
      cy.url().should('include', '/welcome/find-a-course') 
    })

  })

})
